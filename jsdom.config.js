var jsdom = require('jsdom');
const {JSDOM} = jsdom;
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

const {document} = (new JSDOM('')).window;
global.document = document;

function copyProps(src, target) {
    const props = Object.getOwnPropertyNames(src)
    .filter(prop => typeof target[prop] === 'undefined')
    .reduce((result, prop) => ({
        ...result,
        [prop]: Object.getOwnPropertyDescriptor(src, prop),
    }), {});
    Object.defineProperties(target, props);
}
copyProps(document.defaultView, global);